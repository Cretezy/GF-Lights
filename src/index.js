import huejay from "huejay";
import express from "express";

import {
  HUE_BRIDGE_IP,
  HUE_BRIDGE_PORT,
  HUE_BRIDGE_USERNAME,
  HUE_GROUPS,
  HUE_LIGHTS,
  PORT
} from "./config";
import * as R from "ramda";
import template from "./template";
import { flash } from "./sequences";

const mode = process.argv[2];

async function main() {
  if (mode === "discover") {
    const bridges = await huejay.discover();
    bridges.forEach(bridge => {
      console.log(`Id: ${bridge.id}, IP: ${bridge.ip}`);
    });
    return;
  }

  const client = new huejay.Client({
    host: HUE_BRIDGE_IP,
    username: HUE_BRIDGE_USERNAME,
    port: HUE_BRIDGE_PORT
  });

  if (mode === "user") {
    const user_ = new client.users.User();
    user_.deviceType = "Girlfriend";

    const user = await client.users.create(user_);

    console.log(`Using username ${user.username}`);
    return;
  }
  if (mode === "groups") {
    const groups = await client.groups.getAll();
    groups.forEach(group => {
      console.log(`Group [${group.id}]: ${group.name}`);
      console.log(`  Type: ${group.type}`);
      console.log(`  Class: ${group.class}`);
      console.log("  Light Ids: " + group.lightIds.join(", "));
      console.log("  State:");
      console.log(`    Any on:     ${group.anyOn}`);
      console.log(`    All on:     ${group.allOn}`);
      console.log("  Action:");
      console.log(`    On:         ${group.on}`);
      console.log(`    Brightness: ${group.brightness}`);
      console.log(`    Color mode: ${group.colorMode}`);
      console.log(`    Hue:        ${group.hue}`);
      console.log(`    Saturation: ${group.saturation}`);
      console.log(`    X/Y:        ${group.xy?.[0]}, ${group.xy?.[1]}`);
      console.log(`    Color Temp: ${group.colorTemp}`);
      console.log(`    Alert:      ${group.alert}`);
      console.log(`    Effect:     ${group.effect}`);

      if (group.modelId !== undefined) {
        console.log(`  Model Id: ${group.modelId}`);
        console.log(`  Unique Id: ${group.uniqueId}`);
        console.log("  Model:");
        console.log(`    Id:           ${group.model.id}`);
        console.log(`    Manufacturer: ${group.model.manufacturer}`);
        console.log(`    Name:         ${group.model.name}`);
        console.log(`    Type:         ${group.model.type}`);
      }

      console.log();
    });
    return;
  }
  if (mode === "lights") {
    const lights = await client.lights.getAll();
    lights.forEach(light => {
      console.log(`Light [${light.id}]: ${light.name}`);
      console.log(`  Type:             ${light.type}`);
      console.log(`  Unique ID:        ${light.uniqueId}`);
      console.log(`  Manufacturer:     ${light.manufacturer}`);
      console.log(`  Model Id:         ${light.modelId}`);
      console.log("  Model:");
      console.log(`    Id:             ${light.model.id}`);
      console.log(`    Manufacturer:   ${light.model.manufacturer}`);
      console.log(`    Name:           ${light.model.name}`);
      console.log(`    Type:           ${light.model.type}`);
      console.log(`    Color Gamut:    ${light.model.colorGamut}`);
      console.log(`    Friends of Hue: ${light.model.friendsOfHue}`);
      console.log(`  Software Version: ${light.softwareVersion}`);
      console.log("  State:");
      console.log(`    On:         ${light.on}`);
      console.log(`    Reachable:  ${light.reachable}`);
      console.log(`    Brightness: ${light.brightness}`);
      console.log(`    Color mode: ${light.colorMode}`);
      console.log(`    Hue:        ${light.hue}`);
      console.log(`    Saturation: ${light.saturation}`);
      console.log(`    X/Y:        ${light.xy?.[0]}, ${light.xy?.[1]}`);
      console.log(`    Color Temp: ${light.colorTemp}`);
      console.log(`    Alert:      ${light.alert}`);
      console.log(`    Effect:     ${light.effect}`);
      console.log();
    });
    return;
  }

  const groupIds = (HUE_GROUPS || "")
    .split(",")
    .map(R.trim)
    .filter(Boolean);
  const groups = await Promise.all(
    groupIds.map(groupId => client.groups.getById(groupId))
  );

  const lightIds = [
    ...R.flatten(groups.map(R.prop("lightIds"))),
    ...(HUE_LIGHTS || "")
      .split(",")
      .map(R.trim)
      .filter(Boolean)
  ];

  const lights = await Promise.all(
    lightIds.map(lightId => client.lights.getById(lightId))
  );

  async function applyAll(fn) {
    await Promise.all(
      lights.map(async (light, index) => {
        fn(light, index);
        return await client.lights.save(light);
      })
    );
  }
  async function wait(duration) {
    await new Promise(resolve => {
      setTimeout(resolve, duration);
    });
  }

  let running = false;
  async function run() {
    running = true;
    const startSettings = await Promise.all(
      lights.map(R.pick(["on", "brightness", "hue", "saturation"]))
    );

    await applyAll(light => {
      light.on = true;
    });

    const { transitionTime, sequence } = flash();
    for (let action of sequence) {
      if (R.is(Number, action)) {
        await wait(action);
      } else if (R.is(Function, action)) {
        await applyAll(light => {
          if (transitionTime || transitionTime === 0) {
            light.transitionTime = transitionTime;
          }
          action(light);
        });
      }
    }

    await applyAll((light, index) => {
      const startSetting = startSettings[index];
      light.on = startSetting.on;
      light.brightness = startSetting.brightness;
      if (transitionTime || transitionTime === 0) {
        light.transitionTime = transitionTime;
      }
      if (startSetting.hue !== undefined) {
        light.hue = startSetting.hue;
      }
      if (startSetting.saturation !== undefined) {
        light.saturation = startSetting.saturation;
      }
    });
    running = false;
  }

  // Server
  const app = express();

  app.get("/", (req, res) => {
    res.send(
      template(` 
        <form action="/run" method="post">
          Run lights:
          <button type="submit">Run</button>
        </form>
      `)
    );
  });

  app.post("/run", (req, res) => {
    // language=HTML
    res.send(
      template(`
        ${running ? "Already running..." : "Running..."}
        <script>
          setTimeout(function(){
            window.location.replace("/");
          }, ${running ? 2000 : 5000});
        </script>
      `)
    );

    if (!running) {
      run();
    }
  });

  app.listen(PORT);
  console.log(`Server running at :${PORT}`);
}

main();
