export default body => `
  <!doctype html>
    <html lang="en">
    <head>
      <meta charset="UTF-8">
      <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
      <title>Lights</title>
    </head>
    <body>
      <div class="container">
          ${body}
      </div>
      <style>
        body, html{
          font-family: Helvetica Neue, Helvetica, Arial, sans-serif;
          margin: 5%;
        }
        .container{
          margin: 0 auto;
          padding: 50px;
          max-width: 500px;
          background-color: #ebebeb;
          border-radius: 10px;
          text-align: center;
        }
      </style>
    </body>
    </html>
`;
