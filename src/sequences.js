const high = light => {
  light.brightness = 254;
};
const low = light => {
  light.brightness = 50;
};

export const flash = () => ({
  transitionTime: 0.1,
  sequence: [
    high,
    200,
    low,
    200,
    high,
    200,
    low,
    200,
    high,
    200,
    low,
    200,
    high,
    200,
    low,
    200,
    high,
    200,
    low,
    200,
    high,
    200,
    low,
    200
  ]
});
