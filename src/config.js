import dotenv from "dotenv";
dotenv.load();

export const {
  HUE_BRIDGE_IP,
  HUE_BRIDGE_PORT,
  HUE_BRIDGE_USERNAME,
  HUE_GROUPS,
  HUE_LIGHTS
} = process.env;

export const PORT = parseInt(process.env.PORT) || 3000;
