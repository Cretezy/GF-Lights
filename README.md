# GF-Lights

Play sequences on Heu from web.

## Usage

```bash
yarn start [mode]
```

Modes:

- None: Start normally with web server
- `discover`: Find bridges
- `user`: Create user on bridge
- `groups`: Find groups (rooms) on bridge
- `lights`: Find lights on bridge

## Configuration

Copy `.env.template` to `.env` and edit.
